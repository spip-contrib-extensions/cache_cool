<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

return [
	// C
	'cache_cool_description' => 'Servir le cache froid pour aller plus vite, et calculer en différé en tâche de fond',
	'cache_cool_slogan' => 'Servir les pages au plus vite !',
];