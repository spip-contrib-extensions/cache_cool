# Changelog du plugin CacheCool

## 2.1.1 - 2025-02-27

### Fixed

- Chaînes de langue au format SPIP 4.1+

## 2.1.0 - 2024-07-05

### Changed

- Compatible SPIP 4.*

## 2.0.0 - 2024-01-21

### Changed

- Compatible SPIP 4.2 minimum.